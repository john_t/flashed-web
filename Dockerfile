FROM rust:bullseye AS builder
USER root
WORKDIR /app

RUN rustup target add wasm32-unknown-unknown
RUN wget -qO- https://github.com/thedodd/trunk/releases/download/v0.21.0/trunk-x86_64-unknown-linux-gnu.tar.gz | tar -xzf-
COPY . .
RUN echo $PATH
RUN cargo --version
RUN env RUSTFLAGS=--cfg=web_sys_unstable_apis ./trunk build --release
RUN cp public/index.html public/404.html
