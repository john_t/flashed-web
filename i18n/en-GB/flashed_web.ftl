complete-message = Deck Completed!

# Top Bar
go-back = Go Back
share = Share
copy-me = Copy Me!

# Button Box
incorrect = Incorrect
flip = Flip
check = Check
correct = Correct
next = Next

# Errors
save-failed-message = Cannot Save { $error }!
unamed = Unnamed

# Settings
settings = Settings
settings-save-scores = Save Scores:
settings-load-scores = Load Scores:
settings-zeroize = Harsh Mode:
settings-subset-deck = Subset Deck:

# File Management
decks = Decks
deck = Deck
upload = Upload
download = Download
download-scores = Download with scores
new = New
create-new = Create New
create = Create
delete = Delete
edit = Edit
stats = See Stats
folder-new-name = Enter the name for the new folder
more = More
move = Move
move-prompt = Where would you like to move this?
rename-prompt = Enter the new name:
unpack-scores = This directory may contain scores data for decks. Should this be saved too?
move-fail = Move failed
rename = Rename

# Deck editing
settings-name = Name
question = Question
answer = Answer

# Cards
card-delete = Delete Card
card-add = Add Card
card-toggle-input = Toggle Input
save = Save
preview = Preview

# Info Bar
info-card-score = Card Score: { $value }
info-correct = Correct: { $value }
info-remaining = Still to go: { $value }
info-total = Total: { $value }

# Delete
confirm-delete-deck = Are you sure you want to delete { $value }?

import-deck = Import Deck "{ $value }"?
yes = Yes
no = No

migrations-applied = Some migration updates have been applied!
