complete-message = Deck skończony!

# Top Bar
go-back = Przejdź wstecz
download = Pobierać  

# Button Box
incorrect = Złe 
flip = Przewróć 
correct = Poprawnie 

# Errors
save-failed-message = Nie można zapisać { $error }!
unamed = bez nazwy 

# Settings
settings = Ustawienia 
settings-save-scores = Zapisać Wyniki:
settings-load-scores = Ładuj Wyniki:
settings-subset-deck = Subset Deck:

# Deck Management
decks = Decki
upload = Dodaj z files
new = Nowe 
create-new = Stwórz Nowy Deck
create = Stwórz 
delete = Usuń Deck  
edit = Redagować Deck

# Deck editing
settings-name = Nazwa 
question = Pytanie 
answer = Odpowiedz 

# Cards
card-delete = Usuń Kartę 
card-add = Dodaj Kartę 
save = Zapisać 

# Info Bar
info-card-score = Wyniki Kartę: { $value }
info-correct = Poprawnie: { $value }
info-remaining = Jeszcze Do Zrobienia: { $value }
info-total = Suma: { $value }
