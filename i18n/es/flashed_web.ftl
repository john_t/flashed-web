complete-message = ¡Baraja Terminado!

# Top Bar
go-back = Ir Anterior
download = Descargar

# Button Box
incorrect = Incorrecto
flip = Voltear
correct = Correcto

# Errors
save-failed-message = ¡No Se Puede Guardar { $error }!
unamed = Sin Nombre

# Settings
settings = Preferencias
settings-save-scores = Guardar Tantos:
settings-load-scores = Cargar Tantos:
settings-zeroize = Modo Duro:
settings-subset-deck = Subconjunto de Baraja:

# Deck Management
decks = Barajas
upload = Subir
new = Nuevo
create-new = Hacer Baraja Nueva
create = Hacer
delete = Borrar Baraja
edit = Editar Baraja

# Deck editing
settings-name = Nombre
question = Pregunta
answer = Respuesta

# Cards
card-delete = Borrar Carta
card-add = Añadir Carta
save = Guardar

# Info Bar
info-card-score = Tanto de Carta: { $value }
info-correct = Correcto: { $value }
info-remaining = Restante: { $value }
info-total = Total: { $value }
