complete-message = cōnfēcistum fasciculum!

# Top Bar
go-back = redīre
download = capere

# Button Box
incorrect = perpera
flip = vertere
correct = corrēcta

# Errors
save-failed-message = non potest servāre { $error }
unamed = nomen sine

# Settings
settings = optiōnēs
settings-save-scores = notāre:
settings-load-scores = legīre notāvit:
settings-zeroize = lūdus serverus:
settings-subset-deck = fasciculus parvus:

# Deck Management
decks = fasciculī
upload = dar
new = novus
create-new = fasciculum novum facēre
create = facēre
delete = fasciculus dēlēre
edit = fasciculus corigere

# Deck editing
settings-name = novem
question = rogātiō
answer = responsum

# Cards
card-delete = tabula dēlēre
card-add = tabula addere
save = servāre 

# Info Bar
info-card-score = tabula nota: { $value }
info-correct = corrēctī: { $value }
info-remaining = remanēns: { $value }
info-total = totus: { $value }
