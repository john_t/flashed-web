mod idb;
use flashed::Opts;
pub use idb::*;
mod names;
pub use names::{get_download_name, get_file_stem, get_title};
mod copy;
pub mod share;
pub use copy::*;

#[must_use]
pub fn default_opts() -> Opts {
    Opts {
        input: std::path::PathBuf::new(),
        nowrite: false,
        reset: false,
        testing: None,
        zeroize: false,
    }
}
