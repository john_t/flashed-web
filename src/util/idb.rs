use std::collections::BTreeMap;

use flashed::{Card, CardInner, Opts, Scores};
use rexie::{ObjectStore, Rexie, TransactionMode};
use serde::{Deserialize, Serialize};
use wasm_bindgen::JsValue;

use crate::dirs::DirContents;
use crate::pages::{UrlPath, UrlString};
use crate::util;

// I would prefer to use u64 but alas no,
// javascript will through a hissy fit.
pub type FileId = u32;

/// Creates a function that returns an option of
/// the input and displays an error using gloo if it all
/// goes to custard.
macro_rules! fn_o {
    ($f: ident, $x: ident, $t: ty, ($($arg:ident : $arg_ty:ty),*)) => {
        pub async fn $x($($arg: $arg_ty,)*) -> Option<$t> {
            match $f($($arg,)*).await {
                Ok(o) => Some(o),
                Err(e) => {
                    gloo_dialogs::alert(&e.to_string());
                    None
                }
            }
        }
    }
}

fn_o!(build_database, build_database_o, Rexie, ());

pub async fn build_database() -> rexie::Result<Rexie> {
    log::debug!("Creating a database...");
    let rexie = Rexie::builder("flashed-web")
        .version(2)
        .add_object_store(ObjectStore::new("decks"))
        .add_object_store(ObjectStore::new("opts"))
        .add_object_store(ObjectStore::new("scores"))
        .add_object_store(ObjectStore::new("directories"))
        .build()
        .await?;
    log::info!("Created a database.");

    // Ensure dir 0 (`/`) is made
    let transaction =
        rexie.transaction(&["directories"], TransactionMode::ReadWrite)?;
    let directories = transaction.store("directories")?;
    let dir = directories.get(&to_id(0)).await?;
    if dir.is_undefined() {
        let dir = Dir::default();
        let dir = serde_wasm_bindgen::to_value(&dir).unwrap();
        directories.add(&dir, Some(&to_id(0))).await?;
    }

    Ok(rexie)
}

/// Saves a deck to IDB
pub async fn save_deck(
    rexie: &Rexie,
    deck: &[CardInner],
    id: FileId,
) -> rexie::Result<()> {
    log::info!("Saving deck... {}", id);
    let transaction =
        rexie.transaction(&["decks"], TransactionMode::ReadWrite)?;
    let decks = transaction.store("decks")?;
    let deck = serde_wasm_bindgen::to_value(&deck).unwrap();
    decks.put(&deck, Some(&to_id(id))).await?;
    log::info!("Saved deck!");

    Ok(())
}

fn_o!(
    save_deck,
    save_deck_o,
    (),
    (rexie: &Rexie, deck: &[CardInner], id: FileId)
);

pub async fn get_deck(
    rexie: &Rexie,
    deck: FileId,
) -> rexie::Result<Vec<CardInner>> {
    let transaction =
        rexie.transaction(&["decks"], TransactionMode::ReadOnly)?;
    let decks = transaction.store("decks")?;
    let deck = decks.get(&to_id(deck)).await?;
    Ok(serde_wasm_bindgen::from_value(deck).unwrap())
}

fn_o!(get_deck, get_deck_o, Vec<CardInner>, (rexie: &Rexie, deck: FileId));

pub async fn get_deck_r(
    rexie: &Rexie,
    file: FileId,
    root: UrlPath,
) -> rexie::Result<Vec<Card>> {
    let mut cards = vec![];
    match get_directory_from_id(rexie, file).await {
        Ok(opt) => {
            match opt {
                Some(dir) => {
                    for pair in dir.iter() {
                        let mut new_cards = Box::pin(get_deck_r(
                            rexie,
                            pair.1.id(),
                            (root.to_string() + "/" + pair.0)
                                .parse::<UrlPath>()
                                .unwrap(),
                        ))
                        .await?;
                        cards.append(&mut new_cards);
                    }
                }
                None => {
                    // cards.append(&mut get_deck(rexie, file).await?)
                    let mut new_cards = get_deck(rexie, file).await?;
                    let mut new_cards = new_cards
                        .iter_mut()
                        .map(|x| Card {
                            inner: x.clone(),
                            path: root.clone().into(),
                            score: 0,
                            due_date: None,
                        })
                        .collect::<Vec<Card>>();
                    cards.append(&mut new_cards);
                }
            }
        }
        Err(e) => return Err(e),
    }
    Ok(cards)
}

fn_o!(get_deck_r, get_deck_r_o, Vec<Card>, (rexie: &Rexie, deck: FileId, root: UrlPath));

pub async fn delete_deck(rexie: &Rexie, deck: FileId) -> rexie::Result<()> {
    let transaction =
        rexie.transaction(&["decks"], TransactionMode::ReadWrite)?;
    let decks = transaction.store("decks")?;
    decks.delete(&to_id(deck)).await?;
    Ok(())
}

fn_o!(delete_deck, delete_deck_o, (), (rexie: &Rexie, deck: FileId));

/// Saves the opts to IDB
pub async fn save_opts(rexie: &Rexie, opts: &Opts) -> rexie::Result<()> {
    log::info!("Writing opts...");
    let transaction =
        rexie.transaction(&["opts"], TransactionMode::ReadWrite)?;
    let opts_table = transaction.store("opts")?;
    let opt = serde_wasm_bindgen::to_value(&opts).unwrap();
    opts_table.put(&opt, Some(&"flashed".into())).await?;

    Ok(())
}

fn_o!(get_opts, get_opts_o, Opts, (rexie: &Rexie));
fn_o!(save_opts, save_opts_o, (), (rexie: &Rexie, opts: &Opts));

/// Gets the opts from IDB
pub async fn get_opts(rexie: &Rexie) -> rexie::Result<Opts> {
    let transaction =
        rexie.transaction(&["opts"], TransactionMode::ReadOnly)?;
    let opts_table = transaction.store("opts")?;
    let opts = opts_table.get(&"flashed".into()).await.unwrap_or_default();
    Ok(serde_wasm_bindgen::from_value(opts)
        .unwrap_or_else(|_| util::default_opts()))
}

/// Saves the scores to IDB
pub async fn save_scores(
    rexie: &Rexie,
    deck: FileId,
    scores: &Scores,
) -> rexie::Result<()> {
    log::debug!("Writing scores... {}", deck);

    let transaction =
        rexie.transaction(&["scores"], TransactionMode::ReadWrite)?;
    let scores_table = transaction.store("scores")?;
    let scores = serde_wasm_bindgen::to_value(&scores).unwrap();
    scores_table.put(&scores, Some(&to_id(deck))).await?;

    Ok(())
}

fn_o!(
    save_scores,
    save_scores_o,
    (),
    (rexie: &Rexie, deck: FileId, scores: &Scores)
);

/// Gets the scores from the IDB
pub async fn get_scores(
    rexie: &Rexie,
    file: FileId,
) -> rexie::Result<Option<Scores>> {
    let transaction =
        rexie.transaction(&["scores"], TransactionMode::ReadOnly)?;
    let scores_table = transaction.store("scores")?;
    let scores = scores_table.get(&to_id(file)).await?;
    if scores.is_undefined() {
        Ok(None)
    } else {
        Ok(serde_wasm_bindgen::from_value(scores).unwrap())
    }
}

fn_o!(
    get_scores,
    get_scores_o,
    Option<Scores>,
    (rexie: &Rexie, deck: FileId)
);

pub async fn get_scores_r(
    rexie: &Rexie,
    file: FileId,
    root: UrlPath,
) -> rexie::Result<BTreeMap<UrlPath, Scores>> {
    let mut scores = BTreeMap::new();
    match get_directory_from_id(rexie, file).await {
        Ok(opt) => {
            match opt {
                Some(dir) => {
                    for pair in dir.iter() {
                        let mut sub_path = root.clone();
                        sub_path.0.push(UrlString(pair.0.clone()));
                        scores.append(
                            &mut Box::pin(get_scores_r(
                                rexie,
                                pair.1.id(),
                                sub_path,
                            ))
                            .await?,
                        );
                    }
                }
                None => {
                    match get_scores(rexie, file).await? {
                        Some(s) => {
                            scores.insert(root, s);
                        }
                        None => {
                            // panic!("You somehow are getting scores that don't exist. Hjelp.")
                        }
                    }
                }
            }
        }
        Err(e) => return Err(e),
    }

    Ok(scores)
}

fn_o!(
    get_scores_r,
    get_scores_r_o,
    BTreeMap<UrlPath, Scores>,
    (rexie: &Rexie, deck: FileId, root: UrlPath)
);
/// Gets the directory from the IDB
pub async fn get_directory(
    rexie: &Rexie,
    path: impl Iterator<Item = &str>,
) -> rexie::Result<Option<(FileId, Dir)>> {
    let transaction =
        rexie.transaction(&["directories"], TransactionMode::ReadOnly)?;
    let directories_table = transaction.store("directories")?;
    let mut dir = directories_table.get(&to_id(0)).await?;
    let mut dir_id = 0u32;

    // Go through each path segment
    for seg in path {
        if dir.is_undefined() {
            return Ok(None);
        } else {
            let parsed_dir: Dir = serde_wasm_bindgen::from_value(dir).unwrap();
            let file = parsed_dir.get(seg);
            match file {
                Some(File::Deck(_)) => {
                    return Ok(None);
                }
                Some(File::Folder(id)) => {
                    dir = directories_table.get(&to_id(*id)).await?;
                    dir_id = *id;
                }
                None => {
                    return Ok(None);
                }
            }
        }
    }
    if dir.is_undefined() {
        Ok(None)
    } else {
        Ok(Some((dir_id, serde_wasm_bindgen::from_value(dir).unwrap())))
    }
}

fn_o!(
    get_directory,
    get_directory_o,
    Option<(FileId, Dir)>,
    (rexie: &Rexie, path: impl Iterator<Item = &str>)
);

/// Gets a file from the IDB
pub async fn get_file(
    rexie: &Rexie,
    path: &UrlPath,
) -> rexie::Result<Option<FileId>> {
    // Get the directory
    let len_m_1 = path.0.len().saturating_sub(1);
    let dir =
        get_directory(rexie, path.0[..len_m_1].iter().map(|x| x.as_str()))
            .await?;

    let dir = match dir {
        Some((_id, dir)) => dir,
        None => return Ok(None),
    };

    // Get the file
    let file_name = &path.0.last();
    match file_name {
        Some(file_name) => {
            let file = dir.get(&file_name.0);
            Ok(file.map(|x| x.id()))
        }
        None => Ok(None),
    }
}

fn_o!(
    get_file,
    get_file_o,
    Option<FileId>,
    (rexie: &Rexie, path: &UrlPath)
);

/// Gets a directory from the IDB
pub async fn get_directory_from_id(
    rexie: &Rexie,
    id: FileId,
) -> rexie::Result<Option<Dir>> {
    let transaction =
        rexie.transaction(&["directories"], TransactionMode::ReadOnly)?; // Begins Rexie Transaction
    let dirs_table = transaction.store("directories")?; // Gets the directories table from the database
    let dirs = dirs_table.get(&to_id(id)).await?; // Gets the JsValue of the directory at the ID from the table
    if dirs.is_undefined() {
        Ok(None) // Returns either nothing...
    } else {
        Ok(serde_wasm_bindgen::from_value(dirs).unwrap()) // ...or the directory.
    }
}

/// Adds a file to a directory
pub async fn add_file(
    rexie: &Rexie,
    dir_id: FileId,
    file: File,
    name: String,
) -> rexie::Result<()> {
    let transaction =
        rexie.transaction(&["directories"], TransactionMode::ReadWrite)?; // Begins Rexie transaction
    let directories_table = transaction.store("directories")?; // Gets the directories table from the database
    let dir = directories_table.get(&to_id(dir_id)).await?; // Gets the JsValue of the directory at the ID from the table
    let mut dir: Dir = serde_wasm_bindgen::from_value(dir).unwrap(); // Gets the actual directory from the JsValue
    dir.insert(name, file); // Inserts the file into the directory
    let dir = serde_wasm_bindgen::to_value(&dir).unwrap(); // Gets the JsValue of the updated directory
    directories_table.put(&dir, Some(&to_id(dir_id))).await?; // Puts the JsValue into the table

    Ok(()) // Drops everything
}

fn_o!(
    add_file,
    add_file_o,
    (),
    (rexie: &Rexie, dir_id: FileId, file: File, name: String)
);

/// Removes a file from a directory
pub async fn rm_file(
    rexie: &Rexie,
    dir_id: FileId,
    name: &str,
) -> rexie::Result<()> {
    let transaction =
        rexie.transaction(&["directories"], TransactionMode::ReadWrite)?; // Begins Rexie transaction
    let directories_table = transaction.store("directories")?; // Gets the directories table from the database
    let dir = directories_table.get(&to_id(dir_id)).await?; // Gets the JsValue of the directory at the ID from the table
    let mut dir: Dir = serde_wasm_bindgen::from_value(dir).unwrap(); // Gets the actual directory from the JsValue
    dir.remove(name); // Removes the file from the directory
    let dir = serde_wasm_bindgen::to_value(&dir).unwrap(); // Gets the JsValue of the updated directory
    directories_table.put(&dir, Some(&to_id(dir_id))).await?; // Puts the JsValue into the table

    Ok(()) // Drops everything
}

fn_o!(
    rm_file,
    rm_file_o,
    (),
    (rexie: &Rexie, dir_id: FileId, name: &str)
);

/// Creates a new empty directory
pub async fn new_dir(rexie: &Rexie, dir_id: FileId) -> rexie::Result<()> {
    log::info!("Saving directory... {}", dir_id);

    let transaction =
        rexie.transaction(&["directories"], TransactionMode::ReadWrite)?; // Begins Rexie transaction
    let directories = transaction.store("directories")?; // Gets the directories table from the database
    let dir = Dir::default(); // Initialises an empty Dir
    let dir = serde_wasm_bindgen::to_value(&dir).unwrap(); // Gets the JsValue of the Dir
    directories.put(&dir, Some(&to_id(dir_id))).await?; // Stores the Dir in the directory

    log::info!("Saved directory!");

    Ok(()) // Drops everything
}

fn_o!(
    new_dir,
    new_dir_o,
    (),
    (rexie: &Rexie, dir_id: FileId)
);

#[derive(Deserialize, Serialize, Debug, Clone, Copy)]
pub enum File {
    Deck(FileId),
    Folder(FileId),
}

impl File {
    pub fn id(&self) -> FileId {
        match self {
            Self::Deck(x) => *x,
            Self::Folder(x) => *x,
        }
    }
}

pub type Dir = BTreeMap<String, File>;

pub fn to_id(id: FileId) -> JsValue {
    JsValue::from(id) // God fucking knows what this one does
}

pub async fn get_dir_contents(
    rexie: &Rexie,
    file: FileId,
    name: String,
    include_scores: bool,
) -> rexie::Result<Option<DirContents>> {
    if let Some(dir) = get_directory_from_id(rexie, file).await? {
        let mut contents = vec![];

        for f in dir {
            contents.push(
                Box::pin(get_dir_contents(
                    rexie,
                    f.1.id(),
                    f.0,
                    include_scores,
                ))
                .await?
                .unwrap(),
            );
        }

        Ok(Some(DirContents::Directory { name, contents }))
    } else if let Ok(cards) = get_deck(rexie, file).await {
        let scores = get_scores(rexie, file).await?;
        Ok(Some(DirContents::File {
            name,
            cards,
            scores,
        }))
    } else {
        Ok(None)
    }
}
fn_o!(
    get_dir_contents,
    get_dir_contents_o,
    Option<DirContents>,
    (rexie: &Rexie, file: FileId, name: String, include_scores: bool)
);

pub async fn construct_dir(
    rexie: &Rexie,
    contents: DirContents,
    dir_id: FileId,
    unpack_scores: bool,
) -> rexie::Result<()> {
    log::info!("Constructing directory entry \"{}\"", contents.name());

    match contents {
        DirContents::File {
            name,
            cards,
            scores,
        } => {
            let id = fastrand::u32(..);
            log::info!("Saving \"{}\" as a deck with id {}", name, id);
            save_deck(rexie, &cards, id).await?;
            add_file(rexie, dir_id, File::Deck(id), name).await?;
            if unpack_scores {
                if let Some(scores) = scores {
                    save_scores(rexie, id, &scores).await?;
                }
            }
        }
        DirContents::Directory { name, contents } => {
            let id = fastrand::u32(..);
            log::info!("Saving \"{}\" as a folder with id {}", name, id);
            new_dir(rexie, id).await?;
            add_file(rexie, dir_id, File::Folder(id), name.clone()).await?;
            for file in contents {
                Box::pin(construct_dir(rexie, file, id, unpack_scores)).await?;
            }
            log::info!("Constructed directory \"{}\"", name)
        }
    }

    Ok(())
}

fn_o!(
    construct_dir,
    construct_dir_o,
    (),
    (rexie: &Rexie, contents: DirContents, dir_id: FileId, unpack_scores: bool)
);
