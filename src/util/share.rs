use wasm_bindgen::prelude::*;

#[wasm_bindgen(module = "/js/share.js")]
extern "C" {
    // A function to share a URL.
    //
    // Please note it returns a bool.
    pub async fn share(title: &str, text: &str, url: &str) -> JsValue;
}
