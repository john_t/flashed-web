use std::path::PathBuf;

use heck::{AsSnakeCase, AsTitleCase};

#[must_use]
pub fn get_file_stem(name: &str) -> String {
    PathBuf::from(name)
        .file_stem()
        .unwrap_or_default()
        .to_string_lossy()
        .into_owned()
}

#[must_use]
pub fn get_download_name(name: &str) -> String {
    let mut file_path = AsSnakeCase(get_file_stem(name)).to_string();
    file_path.push_str(".json");

    file_path
}

#[must_use]
pub fn get_title(name: &str) -> String {
    AsTitleCase(get_file_stem(name)).to_string()
}
