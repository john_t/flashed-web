use wasm_bindgen::prelude::*;

#[wasm_bindgen(module = "/js/copy.js")]
extern "C" {
    pub fn copy(text: &str);
}
