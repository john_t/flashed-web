use std::collections::HashMap;

use flashed::{CardInner, Opts, Scores};
use gloo_storage::{LocalStorage, Storage};
use rexie::TransactionMode;
use wasm_bindgen_futures::spawn_local;

use crate::fl;
use crate::util::{self, to_id, Dir, File};

pub fn run() {
    spawn_local(async move {
        let mut applied = false;
        applied |= local_storage_to_indexdb().await;
        applied |= files_to_folder().await.unwrap();
        if applied {
            gloo_dialogs::alert(&fl!("migrations-applied"));
        }
    });
}

async fn local_storage_to_indexdb() -> bool {
    // Get all the values from LocalStorage
    let decks: gloo_storage::Result<HashMap<String, Vec<CardInner>>> =
        LocalStorage::get("decks");
    let scores: gloo_storage::Result<HashMap<String, Scores>> =
        LocalStorage::get("scores");
    let opts: gloo_storage::Result<Opts> = LocalStorage::get("opts");

    log::debug!("Building database...");
    let rexie = util::build_database().await.unwrap();
    log::debug!("Built database.");
    let mut applied = false;

    log::info!("{:?}", decks);
    log::debug!("Applying decks...");
    let mut ids = HashMap::new();
    if let Ok(decks) = decks {
        applied |= true;
        LocalStorage::delete("decks");
        for (k, deck) in decks {
            let id = fastrand::u32(..);
            ids.insert(k.clone(), id);
            util::save_deck(&rexie, &deck, id).await.unwrap();
            util::add_file(&rexie, 0, File::Deck(id), k).await.unwrap();
        }
    }
    log::debug!("Applied decks.");

    log::debug!("Applying scores...");
    if let Ok(scores) = scores {
        applied |= true;
        LocalStorage::delete("scores");
        for (k, score) in scores {
            let id = ids.get(&k).unwrap();
            util::save_scores(&rexie, *id, &score).await.unwrap();
        }
    }
    log::debug!("Applied scores.");

    log::debug!("Applying opts...");
    if let Ok(opts) = opts {
        applied |= true;
        LocalStorage::delete("opts");
        util::save_opts(&rexie, &opts).await.unwrap();
    }
    log::debug!("Applied opts.");
    applied
}

async fn files_to_folder() -> rexie::Result<bool> {
    let mut applied = false;

    // Set up the DB
    let rexie = util::build_database().await.unwrap();

    // Begin a transaction
    let transaction = rexie.transaction(
        &["directories", "scores", "decks"],
        TransactionMode::ReadWrite,
    )?;
    let directories = transaction.store("directories")?;
    let scores = transaction.store("scores")?;
    let decks = transaction.store("decks")?;
    let all = decks.get_all(None, None, None, None).await?;
    let root = directories.get(&to_id(0)).await?;
    let mut root: Dir = serde_wasm_bindgen::from_value(root).unwrap();

    // Find all the decks with string keys
    for (k, v) in all {
        if k.is_string() {
            applied = true;

            // Get the scores from this deck
            let score = scores.get(&k).await?;

            // Reinsert the deck under an id
            let id = fastrand::u32(..);
            scores.add(&score, Some(&to_id(id))).await?;
            root.insert(k.as_string().unwrap(), File::Deck(id));
            decks.add(&v, Some(&to_id(id))).await?;
            decks.delete(&k).await?;
            scores.delete(&k).await?;
        }
    }

    let root = serde_wasm_bindgen::to_value(&root).unwrap();
    directories.put(&root, Some(&to_id(0))).await?;

    Ok(applied)
}
