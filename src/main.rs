#![deny(unused_must_use)]

pub mod components;
pub mod migrations;
pub mod panic;
mod dirs;
use yew::prelude::*;
use yew_router::prelude::*;
pub mod pages;
pub mod util;
use i18n_embed::{
    fluent::{fluent_language_loader, FluentLanguageLoader},
    DefaultLocalizer, LanguageLoader, Localizer, WebLanguageRequester,
};
use once_cell::sync::Lazy;
pub use pages::Page;
use rust_embed::RustEmbed;

pub static FTL: Lazy<FluentLanguageLoader> = Lazy::new(|| {
    let loader: FluentLanguageLoader = fluent_language_loader!();
    loader.load_fallback_language(&Localisations).unwrap();
    loader
});

#[derive(RustEmbed)]
#[folder = "i18n"]
struct Localisations;

#[macro_export]
macro_rules! fl {
    ($message_id:literal) => {{
        i18n_embed_fl::fl!($crate::FTL, $message_id)
    }};

    ($message_id:literal, $($args:expr),*) => {{
        i18n_embed_fl::fl!($crate::FTL, $message_id, $($args), *)
    }};
}

#[function_component(App)]
pub fn app() -> Html {
    html! {
        <>
            <BrowserRouter>
                <Switch<Page> render = {pages::switch} />
            </BrowserRouter>
        </>
    }
}

fn main() {
    panic::set_once();
    wasm_logger::init(wasm_logger::Config::default());

    let library_localizer = DefaultLocalizer::new(&*FTL, &Localisations);
    let requested_languages = WebLanguageRequester::requested_languages();

    if let Err(error) = library_localizer.select(&requested_languages) {
        log::error!(
            "Error while loading languages for library_fluent {}",
            error
        );
    }

    migrations::run();
    yew::Renderer::<App>::new().render();
}
