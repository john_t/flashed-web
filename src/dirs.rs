use flashed::{CardInner, Scores};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub enum DirContents {
    File {
        name: String,
        cards: Vec<CardInner>,
        scores: Option<Scores>,
    },
    Directory {
        name: String,
        contents: Vec<DirContents>,
    },
}

impl DirContents {
    pub fn name(&self) -> String {
        match self {
            DirContents::File {
                name,
                cards: _,
                scores: _,
            } => name.to_string(),
            DirContents::Directory { name, contents: _ } => name.to_string(),
        }
    }
}
