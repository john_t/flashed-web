use std::rc::Rc;

use enclose::enclose;
use flashed::CardInner;
pub use flashed::Opts;
use rexie::Rexie;
use wasm_bindgen::JsCast;
use web_sys::{ EventTarget, HtmlInputElement, HtmlAnchorElement };
use yew::prelude::*;
use yew_router::prelude::*;

use crate::components::{Deck, FileUploadButton, Folder, Path, PopUpButton};
use crate::dirs::DirContents;
use crate::fl;
use crate::pages::{Page, UrlPath, UrlString};
use crate::util::{self, get_dir_contents, get_file, Dir, File, FileId};
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

pub enum Msg {
    InitRexie(Rexie),
    RefreshRexie,
    InitOpts(Opts),
    InitDecks(FileId, Dir),
    ToggleNowrite,
    ToggleSaveScores,
    ToggleZeroize,
    SetTesting(usize),
    ToGame(String),
    ToEdit(String),
    ToStats(String),
    Remove(String),
    Move(String),
    Rename(String),
    Download(String, bool),
    Blob(String, String),
    ConfirmMove(String),
    ConfirmRename { old: String, new: String },
    OnLoad(String, String),
    ToFolder(UrlPath),
    Redraw,
    NewFolder,
    Null,
}

#[derive(PartialEq, Properties)]
pub struct OptsProps {
    pub path: UrlPath,
}

pub struct OptsPage {
    pub rexie: Option<Rc<Rexie>>,
    pub opts: Option<Opts>,
    pub decks: Option<Dir>,
    pub id: FileId,
    /// We use this to reload when necassary
    pub last_path: u64,
}

impl OptsPage {
    fn save_opts(&self, ctx: &Context<Self>) {
        ctx.link().send_future(enclose!((self.rexie.as_ref().unwrap() => rexie, self.opts => opts) async move {
            util::save_opts_o(&rexie, opts.as_ref().unwrap()).await;
            Msg::Null
        }));
    }
}

impl Component for OptsPage {
    type Message = Msg;
    type Properties = OptsProps;

    fn create(ctx: &Context<Self>) -> Self {
        ctx.link().send_future(async {
            let rexie = util::build_database().await.unwrap();
            Msg::InitRexie(rexie)
        });
        Self {
            rexie: None,
            opts: None,
            decks: None,
            id: 1,
            last_path: calculate_hash(&ctx.props().path),
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        let navigator = ctx.link().navigator().unwrap();
        match msg {
            Msg::InitRexie(rexie) => {
                self.rexie = Some(Rc::new(rexie));
                ctx.link().callback(|_: ()| Msg::RefreshRexie).emit(());
                false
            }
            Msg::RefreshRexie => {
                self.last_path = calculate_hash(&ctx.props().path);

                // Get the `Opts` from the database
                ctx.link().send_future(enclose!((self.rexie.as_ref().unwrap() => rexie) async move {
                    let opts = util::get_opts(&rexie).await.unwrap_or_else(|_| util::default_opts());
                    Msg::InitOpts(opts)
                }));

                // Get the decks from the database
                let path = ctx.props().path.clone();
                ctx.link().send_future(enclose!((self.rexie.as_ref().unwrap() => rexie) async move {
                    let opts = util::get_directory(&rexie, path.iter())
                            .await
                            .unwrap_or_default()
                            .unwrap_or_default();
                    Msg::InitDecks(opts.0, opts.1)
                }));
                false
            }
            Msg::InitOpts(opts) => {
                self.opts = Some(opts);
                true
            }
            Msg::InitDecks(id, decks) => {
                self.decks = Some(decks);
                self.id = id;
                true
            }
            Msg::ToggleNowrite => {
                self.opts.as_mut().unwrap().nowrite ^= true;
                self.save_opts(ctx);
                true
            }
            Msg::ToggleSaveScores => {
                self.opts.as_mut().unwrap().reset ^= true;
                self.save_opts(ctx);
                true
            }
            Msg::ToggleZeroize => {
                self.opts.as_mut().unwrap().zeroize ^= true;
                self.save_opts(ctx);
                true
            }
            Msg::SetTesting(x) => {
                self.opts.as_mut().unwrap().testing = Some(x);
                self.save_opts(ctx);
                true
            }
            Msg::ToGame(name) => {
                log::info!("ToGame({})", name);
                let mut path = ctx.props().path.clone();
                path.0.push(UrlString(name));
                navigator.push(&Page::Game { path });
                true
            }
            Msg::ToEdit(name) => {
                let mut path = ctx.props().path.clone();
                path.0.push(UrlString(name));
                navigator.push(&Page::Edit { path });
                true
            }
            Msg::ToStats(name) => {
                let mut path = ctx.props().path.clone();
                path.0.push(UrlString(name));
                navigator.push(&Page::Stats { path });
                true
            }
            Msg::Remove(name) => {
                let dir_id = self.id;
                if !gloo_dialogs::confirm(&fl!(
                    "confirm-delete-deck",
                    value = name.clone()
                )) {
                    return false;
                }
                // Remove this from our cache of the decks
                let decks = self.decks.as_mut().unwrap();
                let file_id = decks.remove(&name).map(|x| x.id()).unwrap_or(0);

                // Remove this from localstorage
                ctx.link().send_future(enclose!((self.rexie.as_ref().unwrap() => rexie) async move {
                    util::delete_deck_o(rexie.as_ref(), file_id).await;
                    util::rm_file_o(rexie.as_ref(), dir_id, &name).await;
                    Msg::Redraw
                }));
                true
            }
            Msg::Move(name) => {
                let dir_id = self.id;
                let new_path = gloo_dialogs::prompt(&fl!("move-prompt"), None);
                if let Some(new_path) = new_path {
                    // The unwrap is fine as this is infallible
                    let new_path: UrlPath = new_path.parse().unwrap();

                    let decks = self.decks.as_mut().unwrap();
                    let file = decks.get(&name).copied().unwrap();

                    ctx.link().send_future(enclose!((self.rexie.as_ref().unwrap() => rexie) async move {
                        let mut ok = false;
                        let proc = async {
                            log::debug!("New Path: {:?}", new_path);
                            let dir = util::get_directory(rexie.as_ref(), new_path.iter()).await?;
                            if let Some((dir, _contents)) = dir {
                                util::add_file(rexie.as_ref(), dir, file, name.clone()).await?;
                                util::rm_file(rexie.as_ref(), dir_id, &name).await?;
                                ok = true;
                            } else {
                                gloo_dialogs::alert(&fl!("move-fail"));
                            }
                            Ok::<(), rexie::Error>(())
                        };
                        let res = proc.await;
                        if let Err(e) = res {
                            gloo_dialogs::alert(&e.to_string());
                        }

                        if ok {
                            Msg::ConfirmMove(name)
                        } else {
                            Msg::Null
                        }
                    }));

                    true
                } else {
                    false
                }
            }
            Msg::Rename(name) => {
                let dir_id = self.id;
                let new_name =
                    gloo_dialogs::prompt(&fl!("rename-prompt"), None);
                if let Some(new_name) = new_name {
                    // The unwrap is fine as this is infallible
                    let new_path: UrlPath = new_name.parse().unwrap();

                    let decks = self.decks.as_mut().unwrap();
                    let file = decks.get(&name).copied().unwrap();

                    ctx.link().send_future(enclose!((self.rexie.as_ref().unwrap() => rexie) async move {
                        let mut ok = false;
                        let proc = async {
                            log::debug!("New Path: {:?}", new_path);
                            util::add_file(rexie.as_ref(), dir_id, file, new_name.clone()).await?;
                            util::rm_file(rexie.as_ref(), dir_id, &name).await?;
                            ok = true;
                            Ok::<(), rexie::Error>(())
                        };
                        let res = proc.await;
                        if let Err(e) = res {
                            gloo_dialogs::alert(&e.to_string());
                        }

                        if ok {
                            Msg::ConfirmRename { old: name.clone(), new: new_name.clone() }
                        } else {
                            Msg::Null
                        }
                    }));

                    true
                } else {
                    false
                }
            }
            Msg::ConfirmMove(name) => {
                if let Some(decks) = &mut self.decks {
                    decks.remove(&name);
                }
                true
            }
            Msg::ConfirmRename { old, new } => {
                if let Some(decks) = &mut self.decks {
                    let file = decks.remove(&old);
                    if let Some(file) = file {
                        decks.insert(new, file);
                    }
                }
                true
            }
            Msg::OnLoad(name, data) => {
                match serde_json::from_str::<DirContents>(&data) {
                    Ok(contents) => {
                        let unpack_scores = gloo_dialogs::confirm(&fl!("unpack-scores"));
                        if unpack_scores {
                            log::debug!("Unpacking scores...")
                        } 

                        ctx.link().send_future(enclose!((self.rexie.as_ref().unwrap() => rexie, self.id => dir_id) async move {
                            util::construct_dir_o(&rexie, contents, dir_id, unpack_scores).await;
                            Msg::RefreshRexie
                        }));
                    },
                    Err(_) => {
                        match serde_json::from_str::<Vec<CardInner>>(&data) {
                            Ok(deck) => {
                                // Creates the ID for the file
                                //
                                // Randomness should be good enough
                                //
                                // "Famous last words" - Thesnake66six                
                                let file_id = fastrand::u32(..);
                                let file = File::Deck(file_id);
                                ctx.link().send_future(enclose!((self.rexie.as_ref().unwrap() => rexie, self.id => dir_id) async move {
                                    util::save_deck_o(rexie.as_ref(), &deck, file_id).await;
                                    util::add_file_o(&rexie, dir_id, file, name.clone()).await;
                                    Msg::RefreshRexie
                                }));
                            },
                            Err(e) => gloo_dialogs::alert(&e.to_string()),
                        }
                    },
                }
                false
            }
            Msg::NewFolder => {
                let name = gloo_dialogs::prompt(&fl!("folder-new-name"), None);
                if let Some(name) = name {
                    let file_id = fastrand::u32(..);
                    let file = File::Folder(file_id);
                    let dir_id = self.id;

                    self.decks.as_mut().unwrap().insert(name.clone(), file);
                    ctx.link().send_future(enclose!((self.rexie.as_ref().unwrap() => rexie) async move {
                        util::add_file_o(&rexie, dir_id, file, name).await;
                        util::new_dir_o(&rexie, file_id).await;

                        Msg::Redraw
                    }));
                }
                false
            }
            Msg::ToFolder(path) => {
                navigator.push(&Page::Folder { path });
                true
            }
            Msg::Redraw => true,
            Msg::Null => false,
            Msg::Download(name, include_scores) => {
                use gloo_file::Blob;

                let mut path = ctx.props().path.clone();
                path.0.push(UrlString(name.clone()));

                ctx.link().send_future(enclose!((self.rexie.as_ref().unwrap() => rexie, path, name) async move {
                    log::info!("{}", path);
                    let id = get_file(&rexie, &path).await.unwrap().unwrap();
                    let file = get_dir_contents(&rexie, id, name.clone(), include_scores).await.unwrap().unwrap();
                    // Creates a browser blob
                    let blob = Blob::new_with_options(
                        &*serde_json::to_string(&file).unwrap(),
                        Some("application/json"),
                    );
    
                    // Turns it into a URL
                    let url = web_sys::Url::create_object_url_with_blob(
                        blob.as_ref(),
                    ).unwrap();
                    Msg::Blob(name, url)
                }));

                false
            },
            Msg::Blob(name, url) => {
                let a_element = gloo_utils::document()
                    .create_element("a")
                    .unwrap()
                    .dyn_into::<HtmlAnchorElement>()
                    .unwrap();

                a_element.set_href(&url);
                a_element.set_download(&name);
                gloo_utils::document().body().unwrap().append_child(&a_element).unwrap();
                a_element.click();
                a_element.remove();

                log::info!("{}", url);
                true
            }

        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();
        let _dir_id = self.id;

        // Check the path is correct
        if calculate_hash(&ctx.props().path) != self.last_path {
            log::info!("Redrawing Page::Opts!");
            ctx.link().callback(|_: ()| Msg::RefreshRexie).emit(());
        }

        html! {
            <>
                if let Some(opts) = &self.opts {
                    <div class = "card config-block">
                        <h1> { fl!("settings") } </h1>
                        <div class = "inner">
                            <div>
                                <label for = "write"> { fl!("settings-save-scores") } </label>
                                <input
                                    type = "checkbox"
                                    id = "write"
                                    checked = { !opts.nowrite }
                                    onchange = { link.callback(|_| Msg::ToggleNowrite) }
                                />
                            </div>
                            <div>
                                <label for = "reset"> { fl!("settings-load-scores") } </label>
                                <input
                                    type = "checkbox"
                                    id = "reset"
                                    checked = { !opts.reset }
                                    onchange = { link.callback(|_| Msg::ToggleSaveScores) }
                                />
                            </div>
                            <div>
                                <label for = "zeroize"> { fl!("settings-zeroize") } </label>
                                <input
                                    type = "checkbox"
                                    id = "zeroize"
                                    checked = { opts.zeroize }
                                    onchange = { link.callback(|_| Msg::ToggleZeroize) }
                                />
                            </div>
                            <div>
                                <label for = "testing"> { fl!("settings-subset-deck") } </label>
                                <input
                                    type = "number"
                                    min = "0"
                                    id = "testing"
                                    value = { opts.testing.unwrap_or(0).to_string() }
                                    onchange = {link.batch_callback(|e: Event| {
                                            let target: EventTarget = e.target().unwrap();
                                            Some(Msg::SetTesting(target.unchecked_into::<HtmlInputElement>().value().parse().ok()?))
                                        })
                                    }
                                />
                            </div>
                        </div>
                    </div>
                }
                if let Some(decks) = &self.decks {
                    <div class = "card config-block">
                        <h1> { fl!("decks") } </h1>
                        <Path path = { ctx.props().path.clone() } has_copy = {true}/>
                        <div class = "existing-files"> {
                            decks.iter().map(|(file_name, file_file)| {
                                match file_file {
                                    File::Deck(_deck_id) => html! {
                                        <Deck
                                            name = { file_name.clone() }
                                            onselected = {link.callback(enclose!((file_name) move |_| {Msg::ToGame(file_name.clone())}))}
                                            onedit = {link.callback(enclose!((file_name) move |_| {Msg::ToEdit(file_name.clone())}))}
                                            onremove = {link.callback(enclose!((file_name) move |_| {Msg::Remove(file_name.clone())}))}
                                            onmove = {link.callback(enclose!((file_name) move |_| {Msg::Move(file_name.clone())}))}
                                            onrename = {link.callback(enclose!((file_name) move |_| {Msg::Rename(file_name.clone())}))}
                                            onstats = {link.callback(enclose!((file_name) move |_| {Msg::ToStats(file_name.clone())}))}
                                            ondownload = {link.callback(enclose!((file_name) move |_| {Msg::Download(file_name.clone(), false)}))}
                                            ondownloadscores = {link.callback(enclose!((file_name) move |_| {Msg::Download(file_name.clone(), true)}))}
                                            />
                                        },
                                        File::Folder(_dir_id) => {
                                            let mut new_path = ctx.props().path.clone();
                                            new_path.0.push(UrlString(file_name.clone()));
                                            let onselected = link.callback(move |_| Msg::ToFolder(new_path.clone()));
                                            html! {
                                                <Folder
                                                name = { file_name.clone() }
                                                {onselected}
                                                onremove = {link.callback(enclose!((file_name) move |_| {Msg::Remove(file_name.clone())}))}
                                                onmove = {link.callback(enclose!((file_name) move |_| {Msg::Move(file_name.clone())}))}
                                                onrename = {link.callback(enclose!((file_name) move |_| {Msg::Rename(file_name.clone())}))}
                                                onplay = {link.callback(enclose!((file_name) move |_| {Msg::ToGame(file_name.clone())}))}
                                                ondownload = {link.callback(enclose!((file_name) move |_| {Msg::Download(file_name.clone(), false)}))}
                                                ondownloadscores = {link.callback(enclose!((file_name) move |_| {Msg::Download(file_name.clone(), true)}))}
                                            />
                                        }
                                    }
                                }
                            }).collect::<Html>()
                        } </div>
                        <div class="button-container">
                            <FileUploadButton
                                onload = { link.callback(|(name, data)| Msg::OnLoad(name, data)) }
                                accept = "application/json .json .flashed"
                            >
                                { fl!("upload") }
                            </FileUploadButton>
                            <PopUpButton >
                                <span> {fl!("new")} </span>
                                <div class = "link-button">
                                    <Link<Page> to = { Page::NewDeck { path: ctx.props().path.clone() } }> { fl!("deck") } </Link<Page>>
                                </div>
                                <button onclick = {link.callback(|_| Msg::NewFolder)}> { "Folder" } </button>
                            </PopUpButton>
                        </div>
                    </div>
                }
            </>
        }
    }
}

fn calculate_hash<T: Hash>(t: &T) -> u64 {
    let mut s = DefaultHasher::new();
    t.hash(&mut s);
    s.finish()
}
