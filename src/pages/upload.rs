use std::collections::HashMap;

use flashed::Card;
use gloo_storage::{LocalStorage, Storage};
use yew::prelude::*;
use yew_router::prelude::*;

use crate::fl;
use crate::pages::Page;
use crate::util;

#[derive(Properties, PartialEq, Clone)]
pub struct Props {
    pub name: String,
    pub value: String,
}

#[function_component(Upload)]
pub fn upload(props: &Props) -> Html {
    let history = use_history().unwrap();

    html! {
        <div class = "card-container">
            <div class = "card">
                <h1> { fl!("import-deck", value = props.name.clone()) } </h1>
                <div class = "button-container">
                    <button onclick = {
                        let history = history.clone();
                        Callback::once(move |_| history.push(Page::Opts))
                    }>
                        {fl!("no")}
                    </button>
                    <button
                        class = "good-action"
                        onclick = {
                            let props = props.clone();
                            Callback::once(move |_| {
                                let props = props;
                                let x = move || {
                                    todo!("Add the new deck.");
                                    Ok::<(), String>(())
                                };
                                if let Err(x) = x() {
                                    gloo_dialogs::alert(&x);
                                }
                                history.push(Page::Opts);
                            })
                        }
                    > { fl!("yes") } </button>
                </div>
            </div>
        </div>
    }
}
