use crate::components::Progress;
use flashed::Scores;
use std::rc::Rc;

use enclose::enclose;
use flashed::CardInner;
use rexie::Rexie;
pub use yew::prelude::*;
use yew_lucide::Undo;
pub use yew_router::prelude::*;

use crate::pages::{Page, UrlPath};
use crate::util::{self, FileId};

#[derive(Properties, PartialEq)]
pub struct StatsProps {
    pub path: UrlPath,
}

#[derive(Default)]
pub struct StatsDeck {
    pub rexie: Option<Rc<Rexie>>,
    pub deck: Option<Vec<CardInner>>,
    pub scores: Option<Option<Scores>>,
    pub id: FileId,
}

impl StatsDeck {
    fn name(&self, ctx: &Context<Self>) -> String {
        ctx.props().path.0.last().cloned().unwrap_or_default().0
    }
}

pub enum Msg {
    InitRexie(Rexie),
    InitDeck(FileId, Vec<CardInner>, Option<Scores>),
    OnLoad(String, String),
    Redraw,
}

impl Component for StatsDeck {
    type Message = Msg;
    type Properties = StatsProps;

    fn create(ctx: &Context<Self>) -> Self {
        ctx.link().send_future(async {
            let rexie = util::build_database().await.unwrap();
            Msg::InitRexie(rexie)
        });
        Self::default()
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::InitRexie(rexie) => {
                self.rexie = Some(Rc::new(rexie));

                // Get the deck from the database
                let path = ctx.props().path.clone();
                ctx.link().send_future(enclose!((self.rexie.as_ref().unwrap() => rexie) async move {
                    let id = util::get_file_o(&rexie, &path).await.flatten().unwrap_or_default();
                    let deck = util::get_deck_o(&rexie, id).await.unwrap_or_default();
                    let scores = util::get_scores_o(&rexie, id).await.flatten();
                    Msg::InitDeck(id, deck, scores)
                }));

                true
            }
            Msg::InitDeck(id, deck, scores) => {
                self.deck = Some(deck);
                self.id = id;
                self.scores = Some(scores);
                true
            }
            Msg::OnLoad(_name, data) => {
                match serde_json::from_str::<Vec<CardInner>>(&data) {
                    Ok(deck) => {
                        self.deck = Some(deck.clone());
                        let file_id = self.id;
                        ctx.link().send_future(enclose!((self.rexie.as_ref().unwrap() => rexie) async move {
                            util::save_deck_o(rexie.as_ref(), &deck, file_id).await;
                            Msg::Redraw
                        }));
                    }
                    Err(e) => gloo_dialogs::alert(&e.to_string()),
                }

                true
            }
            Msg::Redraw => true,
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let props = ctx.props();
        let _link = ctx.link();
        let total =
            self.deck.as_ref().map(|x| x.len()).unwrap_or_default() as f32;
        let scores = {
            let mut vec = Vec::with_capacity(5);
            let mut sub_total = 0;
            if let Some(Some(ref scores)) = self.scores {
                for (_card, score) in &scores.scores {
                    vec.resize(vec.len().max(*score + 1), 0);
                    vec[*score] += 1;
                    sub_total += 1;
                }
            }
            vec.resize(vec.len().max(1), 0);
            vec[0] += total as usize - sub_total;
            vec
        };
        html! {
            if let Some(_deck) = &self.deck {
                <>
                    <TopBar
                        name = { self.name(ctx) }
                        path = { props.path.clone() }
                    />
                    <div class = "centered-text margin-bottom">
                        <table class="unpadded">
                            <thead>
                                <tr>
                                    <th> { "Score" } </th>
                                    <th> { "Count" } </th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    scores.into_iter().enumerate().map(|(i, current)| {
                                        let current = current as f32;
                                        html! {
                                            <tr>
                                                <td> {i} </td>
                                                <td> <Progress {total} {current} class="rev"/> </td>
                                            </tr>
                                        }
                                    }).collect::<Html>()
                                }
                            </tbody>
                        </table>
                    </div>
                </>
            }
        }
    }
}

struct TopBar;

#[derive(PartialEq, Properties)]
pub struct TopBarProps {
    pub name: String,
    pub path: UrlPath,
}

impl Component for TopBar {
    type Message = ();
    type Properties = TopBarProps;

    fn create(_ctx: &Context<Self>) -> Self {
        Self
    }

    fn update(&mut self, _ctx: &Context<Self>, _msg: Self::Message) -> bool {
        true
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let _link = ctx.link();
        let props = ctx.props();
        let back_to = {
            let mut path = props.path.clone();
            path.0.pop();
            Page::Folder { path }
        };

        html! {
            <div class = "top-bar">
                <div>
                    <div class = "link-button" aria-label = "Go back">
                        <Link<Page> to = { back_to }> <Undo size = "15px"/> </Link<Page>>
                    </div>
                </div>
                <h1> { props.name.clone() } </h1>
            </div>
        }
    }
}
