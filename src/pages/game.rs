use std::collections::BTreeMap;
use std::mem;
use std::path::PathBuf;
use std::rc::Rc;

use enclose::enclose;
use flashed::{App, CardType, Deck, Opts, Scores};
use rexie::Rexie;
use yew::prelude::*;
use yew_lucide::{Download as DownloadIcon, Undo};
use yew_router::prelude::*;

use crate::components::info_bar;
use crate::components::{Download, PlayingCard};
use crate::fl;
use crate::pages::{Page, UrlPath};
use crate::util::{self, FileId};

#[derive(PartialEq, Properties)]
pub struct GameProps {
    pub path: UrlPath,
}

#[derive(Default)]
pub struct Game {
    pub rexie: Option<Rc<Rexie>>,
    pub opts: Option<Opts>,
    pub deck: Option<Deck>,
    pub scores: Option<BTreeMap<UrlPath, Scores>>,
    pub app: Option<App>,
    pub summing: bool,
}

pub enum Msg {
    InitRexie(Rexie),
    InitOpts(Opts),
    InitDeck(Deck, (FileId, BTreeMap<UrlPath, Scores>), bool),
    Promote,
    Demote,
    Redraw,
    Null,
}

impl Game {
    /// Called once Game has all information it needs
    /// Assembles deck from strut fields
    fn try_make_app(&mut self) {
        if let Some((deck, (scores, _opts))) = self
            .deck
            .as_mut()
            .zip(self.scores.as_mut().zip(self.opts.as_mut()))
        {
            let mut app =
                App::new(mem::take(deck), mem::take(&mut self.opts).unwrap());
            if !app.opts.reset {
                for (path, score) in mem::take(scores) {
                    let path: PathBuf = path.into();
                    app.add_scores(score, &path);
                }
            }
            self.app = Some(app);
        }
    }

    /// Saves scores recursively
    fn save_scores(&self, ctx: &Context<Self>) {
        if self.app.as_ref().unwrap().opts.nowrite {
            return;
        }

        let app = self.app.to_owned().unwrap();
        let paths = app.get_paths();
        for path in paths {
            let scores = make_scores_static(app.scores_from_path(path));

            ctx.link().send_future(
                enclose!((self.rexie => rexie, path) async move {
                    let id = util::get_file_o(rexie.as_ref().unwrap(), &path.clone().to_str().unwrap().parse().unwrap()).await;
                    util::save_scores_o(rexie.as_ref().unwrap(), id.expect("You knew this would happen. It did. src/pages/game.rs ln~76, First expect, 2024-12-16-09-53").expect("You knew this would happen. It did. src/pages/game.rs ln~76, Second expect, 2024-12-16-09-53"), &scores).await;
                    Msg::Null
                }
            ));
        }
    }

    /// Gets the name of the deck????
    /// Fuck knows atp
    fn name(&self, ctx: &Context<Self>) -> String {
        ctx.props().path.0.last().cloned().unwrap_or_default().0
    }
}

impl Component for Game {
    type Message = Msg;
    type Properties = GameProps;

    fn create(ctx: &Context<Self>) -> Self {
        ctx.link().send_future(async {
            let rexie = util::build_database().await.unwrap();
            Msg::InitRexie(rexie)
        });
        Self::default()
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::InitRexie(rexie) => {
                // Called on creation of Game
                self.rexie = Some(Rc::new(rexie));

                // Get the `Opts` from the database
                ctx.link().send_future(enclose!((self.rexie.as_ref().unwrap() => rexie) async move {
                    let opts = util::get_opts(&rexie).await.unwrap_or_else(|_| util::default_opts());
                    Msg::InitOpts(opts)
                }));

                // Get the deck from the database
                let path = ctx.props().path.clone();
                ctx.link().send_future(enclose!((self.rexie.as_ref().unwrap() => rexie) async move {
                    let id = util::get_file_o(&rexie, &path).await.flatten().unwrap_or_default();
                    let summing = util::get_directory(&rexie, path.iter()).await.unwrap().is_some();
                    let deck = util::get_deck_r_o(&rexie, id, path.clone()).await.unwrap_or_default();
                    let scores = (id, util::get_scores_r_o(&rexie, id, path).await.unwrap());
                    Msg::InitDeck(deck.into(), scores, summing)
                }));

                true
            }
            Msg::InitOpts(opts) => {
                self.opts = Some(opts);
                self.try_make_app();
                true
            }
            Msg::InitDeck(deck, scores, summing) => {
                self.deck = Some(deck);
                self.scores = Some(scores.1);
                self.summing = summing;
                self.try_make_app();
                true
            }
            Msg::Promote => {
                let app = self.app.as_mut().unwrap();
                app.change_current_card_score(1);
                app.next_card();
                self.save_scores(ctx);
                true
            }
            Msg::Demote => {
                let app = self.app.as_mut().unwrap();
                app.change_current_card_score(-1);
                app.next_card();
                self.save_scores(ctx);
                true
            }
            Msg::Redraw => true,
            Msg::Null => false,
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let props = ctx.props();
        let link = ctx.link();
        let back_to = {
            let mut path = props.path.clone();
            path.0.pop();
            Page::Folder { path }
        };
        html! {
            if let Some(app) = &self.app {
                if app.unfinished_count() == 0 {
                    <div class="game-container">
                        <div class = "card-container">
                            <div class = "finish-container">
                                <h1> { fl!("complete-message") } </h1>
                                <div class = "good-action link-button">
                                    <Link<Page> to = { back_to }> { "Unload Deck" } </Link<Page>>
                                </div>
                            </div>
                        </div>
                    </div>
                } else {
                    <div class="game-container">
                        <TopBar
                            name = { self.name(ctx) }
                            file = { serde_json::to_string(app.cards()).unwrap() }
                            download_name = { util::get_download_name(&self.name(ctx)) }
                            path = { props.path.clone() }
                        />
                        <div class = "card-container">
                            <PlayingCard
                                input = { app.card().inner.question_type == CardType::Input }
                                question = { app.card().inner.question.clone() }
                                answer = { app.card().inner.answer.clone() }
                                score = { app.card_score() }
                                on_promote = { link.callback(|_| Msg::Promote) }
                                on_demote = { link.callback(|_| Msg::Demote) }
                                path = {
                                    if self.summing {
                                        let path = app.card().path.clone();
                                        let path = path.iter().skip(props.path.0.len()).collect();

                                        Some(path)
                                    } else {
                                        None
                                    }
                                }
                            />
                        </div>
                        { info_bar(app) }
                    </div>
                }
            }
        }
    }
}

#[derive(PartialEq, Properties)]
pub struct TopBarProps {
    pub name: String,
    pub download_name: String,
    pub file: String,
    pub path: UrlPath,
}

#[function_component(TopBar)]
pub fn topbar(props: &TopBarProps) -> Html {
    let back_to = {
        let mut path = props.path.clone();
        path.0.pop();
        Page::Folder { path }
    };

    html! {
        <div class = "top-bar">
            <div>
                <div class = "link-button" aria-label = "Go back">
                    <Link<Page> to = { back_to }> <Undo size = "15px"/> </Link<Page>>
                </div>
            </div>
            <h1> { props.name.clone() } </h1>
            <div class = "right-align">
                <Download
                    file_name = { props.download_name.clone() }
                    file = { props.file.clone() }
                >
                    <DownloadIcon size = "15px" />
                </Download>
                // <Share
                //     file = { props.file.clone() }
                //     name = { props.name.clone() }
                // />
            </div>
        </div>
    }
}

fn make_scores_static(scores: Scores) -> Scores {
    Scores {
        scores: scores
            .scores
            .into_iter()
            .map(|(x, v)| (x.to_owned(), v))
            .collect(),
        dues: scores
            .dues
            .into_iter()
            .map(|(x, v)| (x.to_owned(), v))
            .collect(),
        circulating: scores
            .circulating
            .into_iter()
            .map(|x| x.to_owned())
            .collect(),
    }
}
