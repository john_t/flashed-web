pub mod opts;
use opts::OptsPage;
pub mod game;
use game::Game;
pub mod new_deck;
pub use new_deck::NewDeck;
pub mod edit_deck;
pub use edit_deck::EditDeck;
pub mod stats_deck;
use percent_encoding::{
    percent_decode_str, utf8_percent_encode, NON_ALPHANUMERIC,
};
pub use stats_deck::StatsDeck;
use std::convert::Infallible;
use std::fmt::{self, Display, Formatter, Write};
use std::path::PathBuf;
use std::str::FromStr;
use yew::prelude::*;
use yew_router::prelude::*;

/// Workaround as yew does not support
/// % encoding by default. Which is really annoying.
#[derive(Debug, Clone, PartialEq, Hash, Default, Eq, PartialOrd, Ord)]
pub struct UrlString(pub String);

impl UrlString {
    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }
}

impl FromStr for UrlString {
    type Err = Infallible;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self(percent_decode_str(s).decode_utf8_lossy().to_string()))
    }
}

impl Display for UrlString {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), fmt::Error> {
        utf8_percent_encode(&self.0, NON_ALPHANUMERIC).fmt(f)
    }
}

/// A type which creates a `Vec<String>` from the path
/// This is because it is much easier than not doing so.
#[derive(Debug, Clone, PartialEq, Hash, Default, Eq, PartialOrd, Ord)]
pub struct UrlPath(pub Vec<UrlString>);

impl UrlPath {
    pub fn iter(&self) -> impl Iterator<Item = &str> {
        self.0.iter().map(UrlString::as_str)
    }
}

impl FromStr for UrlPath {
    type Err = Infallible;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let split = s.split('/');
        let mut vec = Vec::new();
        for segment in split {
            let url_string = segment.parse()?;
            vec.push(url_string);
        }
        Ok(Self(vec))
    }
}

impl Display for UrlPath {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), fmt::Error> {
        for (i, segment) in self.0.iter().enumerate() {
            segment.fmt(f)?;
            if i + 1 != self.0.len() {
                f.write_char('/')?;
            }
        }
        Ok(())
    }
}

impl From<UrlPath> for PathBuf {
    fn from(value: UrlPath) -> Self {
        let mut p = PathBuf::new();
        for part in value.iter() {
            p.push(part);
        }
        p
    }
}

impl From<PathBuf> for UrlPath {
    fn from(value: PathBuf) -> Self {
        // let mut p = PathBuf::new();
        // for part in value.iter() {
        //     p.push(part);
        // }
        // p
        UrlPath(
            value
                .components()
                .map(|x| {
                    format!("{:?}", x.as_os_str()).parse::<UrlString>().unwrap()
                })
                .collect(),
        )
    }
}

/// All the pages in the application.
#[derive(Clone, Routable, PartialEq)]
pub enum Page {
    #[at("/")]
    Opts,
    // Because of older redirects :l
    #[at("/index.html")]
    IndexHtml,
    #[at("/folder/*path")]
    Folder { path: UrlPath },
    // This is because wildcad parameters refuse to match
    // on blanks which is really annoying.
    #[at("/folder/")]
    FolderBlank,
    #[at("/game/*path")]
    Game { path: UrlPath },
    #[at("/create/*path")]
    NewDeck { path: UrlPath },
    #[at("/create/")]
    NewDeckBlank,
    #[at("/edit/*path")]
    Edit { path: UrlPath },
    #[at("/stats/*path")]
    Stats { path: UrlPath },
    #[not_found]
    #[at("/404")]
    NotFound,
}

#[must_use]
pub fn switch(page: Page) -> Html {
    match page {
        Page::Opts => html! { <OptsPage path = {UrlPath(vec![])}/> },
        Page::IndexHtml => html! {<Redirect<Page> to = {Page::Opts}/>},
        Page::Folder { path } => html! { <OptsPage {path}/> },
        Page::FolderBlank => html! { <OptsPage path = {UrlPath(vec![])}/> },
        Page::Game { path } => html! {<Game {path}/>},
        Page::NewDeck { path } => html! { <NewDeck {path} /> },
        Page::NewDeckBlank => html! { <NewDeck path = {UrlPath(vec![])}/> },
        Page::Edit { path } => html! {<EditDeck {path}/>},
        Page::Stats { path } => html! {<StatsDeck {path}/>},
        Page::NotFound => html! {<pre> { "404 :(" } </pre>},
    }
}
