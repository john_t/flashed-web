use std::rc::Rc;

use enclose::enclose;
use flashed::{CardInner, CardType};
use rexie::Rexie;
pub use yew::prelude::*;
use yew_lucide::Plus;
use yew_lucide::{Download as DownloadIcon, Eye, EyeOff, Share, Undo};
pub use yew_router::prelude::*;

use crate::components::{
    Download, EditableCard, FileUploadButton, PlayingCard,
};
use crate::fl;
use crate::pages::{Page, UrlPath};
use crate::util::{self, FileId};

#[derive(Properties, PartialEq)]
pub struct EditProps {
    pub path: UrlPath,
}

#[derive(Default)]
pub struct EditDeck {
    pub rexie: Option<Rc<Rexie>>,
    pub deck: Option<Vec<CardInner>>,
    pub show_previews: bool,
    pub id: FileId,
}

impl EditDeck {
    fn name(&self, ctx: &Context<Self>) -> String {
        ctx.props().path.0.last().cloned().unwrap_or_default().0
    }
}

pub enum Msg {
    InitRexie(Rexie),
    InitDeck(FileId, Vec<CardInner>),
    TogglePreviews,
    Redraw,
    Null,
    EditQuestion(usize, String),
    EditAnswer(usize, String),
    InputToggle(usize),
    Remove(usize),
    AddCard,
    Save,
    OnLoad(String, String),
}

impl Component for EditDeck {
    type Message = Msg;
    type Properties = EditProps;

    fn create(ctx: &Context<Self>) -> Self {
        ctx.link().send_future(async {
            let rexie = util::build_database().await.unwrap();
            Msg::InitRexie(rexie)
        });
        Self::default()
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::InitRexie(rexie) => {
                self.rexie = Some(Rc::new(rexie));

                // Get the deck from the database
                let path = ctx.props().path.clone();
                ctx.link().send_future(enclose!((self.rexie.as_ref().unwrap() => rexie) async move {
                    log::info!("{}", path);
                    let id = util::get_file_o(&rexie, &path).await.flatten().unwrap_or_default();
                    let deck = util::get_deck_o(&rexie, id).await.unwrap_or_default();
                    Msg::InitDeck(id, deck)
                }));

                true
            }
            Msg::InitDeck(id, deck) => {
                self.deck = Some(deck);
                self.id = id;
                true
            }
            Msg::TogglePreviews => {
                self.show_previews ^= true;
                true
            }
            Msg::Redraw => true,
            Msg::Null => false,
            Msg::EditQuestion(id, s) => {
                self.deck.as_mut().unwrap()[id].question = s;
                false
            }
            Msg::EditAnswer(id, s) => {
                self.deck.as_mut().unwrap()[id].answer = s;
                false
            }
            Msg::InputToggle(id) => {
                let card = &mut self.deck.as_mut().unwrap()[id];
                card.question_type = match card.question_type {
                    CardType::Input => CardType::Memory,
                    CardType::Memory => CardType::Input,
                };
                true
            }
            Msg::AddCard => {
                self.deck.as_mut().unwrap().push(CardInner {
                    question: String::new(),
                    answer: String::new(),
                    question_type: CardType::Memory,
                });
                true
            }
            Msg::Remove(id) => {
                self.deck.as_mut().unwrap().remove(id);
                true
            }
            Msg::Save => {
                let deck_id = self.id;
                ctx.link().send_future(enclose!((self.rexie.as_ref().unwrap() => rexie, self.deck => deck) async move {
                    util::save_deck_o(&rexie, deck.as_ref().unwrap(), deck_id).await;
                    Msg::Null
                }));
                false
            }
            Msg::OnLoad(_name, data) => {
                match serde_json::from_str::<Vec<CardInner>>(&data) {
                    Ok(deck) => {
                        self.deck = Some(deck.clone());
                        let file_id = self.id;
                        ctx.link().send_future(enclose!((self.rexie.as_ref().unwrap() => rexie) async move {
                            util::save_deck_o(rexie.as_ref(), &deck, file_id).await;
                            Msg::Redraw
                        }));
                    }
                    Err(e) => gloo_dialogs::alert(&e.to_string()),
                }

                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let props = ctx.props();
        let link = ctx.link();
        html! {
            if let Some(deck) = &self.deck {
                <>
                    <TopBar
                        path = { props.path.clone() }
                        name = { self.name(ctx) }
                        file = { serde_json::to_string(deck).unwrap() }
                        download_name = { util::get_download_name(&self.name(ctx)) }
                        on_previews_changed = { link.callback(|_| Msg::TogglePreviews) }
                        on_upload = { link.callback(|(name, data)| Msg::OnLoad(name, data)) }
                        float = { true }
                    />
                    <div class = "centered-text margin-bottom">
                        <div class = "config-block pseudo-card-container margin-top">
                            <div class = "inner list"> {
                                deck.iter().enumerate().map(|(i, card)| {
                                    html! {
                                        if self.show_previews {
                                            <PlayingCard
                                                input = { card.question_type == CardType::Input }
                                                question = { card.question.clone() }
                                                answer = { card.answer.clone() }
                                            />
                                        } else {
                                            <EditableCard
                                                question = { card.question.clone() }
                                                answer = { card.answer.clone() }
                                                input = { card.question_type == CardType::Input }
                                                on_edit_question = {link.callback( move|x| Msg::EditQuestion(i, x)) }
                                                on_edit_answer = { link.callback(move|x| Msg::EditAnswer(i, x)) }
                                                on_input_toggled = { link.callback(move|_| Msg::InputToggle(i)) }
                                                on_remove = { link.callback(move|_| Msg::Remove(i)) }
                                            />
                                        }
                                    }
                                }).collect::<Html>()
                            } </div>
                        </div>
                        <button
                            aria-label = { fl!("card-add") }
                            class = "fullwidth"
                            onclick = { link.callback(|_| Msg::AddCard) }
                        > <Plus/> </button>
                    </div>
                    <div class = "bar bottom-bar-control floating-bar">
                        <button
                            class = "good-action"
                            onclick = { link.callback(|_| Msg::Save) }
                        > { fl!("save") } </button>
                    </div>
                </>
            }
        }
    }
}

#[derive(PartialEq, Properties)]
pub struct TopBarProps {
    pub name: String,
    pub download_name: String,
    pub file: String,
    pub on_previews_changed: Callback<bool>,
    pub on_upload: Callback<(String, String)>,
    pub path: UrlPath,
    pub float: bool,
}

pub struct TopBar {
    pub show_previews: bool,
}

pub enum TopBarMsg {
    SetPreview(bool),
}

impl Component for TopBar {
    type Message = TopBarMsg;
    type Properties = TopBarProps;

    fn create(_ctx: &Context<Self>) -> Self {
        Self {
            show_previews: false,
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Self::Message::SetPreview(previews) => {
                self.show_previews = previews;
                ctx.props().on_previews_changed.emit(previews);
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();
        let props = ctx.props();
        let back_to = {
            let mut path = props.path.clone();
            path.0.pop();
            Page::Folder { path }
        };

        let float = if props.float {
            Some("floating-bar")
        } else {
            None
        };
        let classes = yew::classes!("top-bar", float);

        html! {
            <div class = {classes}>
                <div>
                    <div class = "link-button" aria-label = "Go back">
                        <Link<Page> to = { back_to }> <Undo size = "15px"/> </Link<Page>>
                    </div>
                </div>
                <h1> { props.name.clone() } </h1>
                <div class = "right-align">
                    <Download
                        file_name = { props.download_name.clone() }
                        file = { props.file.clone() }
                    >
                        <DownloadIcon size = "15px"/>
                    </Download>
                    <FileUploadButton
                        onload = { props.on_upload.clone() }
                        accept = "application/json .json .flashed"
                        class = "file-upload-button-circle"
                    > <Share size = "15px"/>
                    </FileUploadButton>
                    // <Share
                    //     name = {props.name.clone()}
                    //     file = { props.file.clone() }
                    // />
                   <button
                       onclick = {
                           let show_previews = self.show_previews;
                           link.callback(
                               move |_| Self::Message::SetPreview(!show_previews)
                           )
                       }
                       aria-label = "Go back"
                   >
                        if self.show_previews {
                            <EyeOff size = "15px"/>
                        } else {
                            <Eye size = "15px"/>
                        }
                    </button>
                </div>
            </div>
        }
    }
}
