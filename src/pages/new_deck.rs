use std::rc::Rc;

use enclose::enclose;
use rexie::Rexie;
use wasm_bindgen::JsCast;
use web_sys::{EventTarget, HtmlInputElement};
pub use yew::prelude::*;
pub use yew_router::prelude::*;

use crate::fl;
use crate::pages::{Page, UrlPath, UrlString};
use crate::util::{self, File, FileId};

#[derive(Properties, PartialEq)]
pub struct Props {
    /// Path of the folder which we are creating
    /// this from
    pub path: UrlPath,
}

pub struct NewDeck {
    pub rexie: Option<Rc<Rexie>>,
    pub string: String,
}

pub enum Msg {
    InitRexie(Rexie),
    SetString(String),
    Proceed(FileId),
    Create,
}

impl Component for NewDeck {
    type Message = Msg;
    type Properties = Props;

    fn create(ctx: &Context<Self>) -> Self {
        ctx.link().send_future(async {
            let rexie = util::build_database().await.unwrap();
            Msg::InitRexie(rexie)
        });
        Self {
            rexie: None,
            string: String::new(),
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        let navigator = ctx.link().navigator().unwrap();
        match msg {
            Msg::InitRexie(rexie) => {
                self.rexie = Some(Rc::new(rexie));
                true
            }
            Msg::SetString(s) => {
                self.string = s;
                false
            }
            Msg::Proceed(_id) => {
                let name = std::mem::take(&mut self.string);
                let mut path = ctx.props().path.clone();
                path.0.push(UrlString(name));
                navigator.push(&Page::Edit { path });
                true
            }
            Msg::Create => {
                let s = self.string.clone();
                let file_id = fastrand::u32(..);
                let path = ctx.props().path.clone();
                ctx.link().send_future(enclose!((self.rexie => rexie) async move {
                    let rexie = rexie.as_ref().unwrap();
                    let dir_id = util::get_directory_o(rexie, path.iter()).await;
                    if let Some(Some((dir_id, _))) = dir_id {
                        util::save_deck_o(rexie, &[], file_id).await;
                        util::add_file_o(rexie, dir_id, File::Deck(file_id), s).await;
                    }
                    Msg::Proceed(file_id)
                }));
                false
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();
        html! {
            if self.rexie.is_some() {
                <div class = "card-container">
                    <div class = "card">
                        <h1> { fl!("create-new") } </h1>
                         <div class = "input-pair">
                             <label for = "name"> { fl!("settings-name") } </label>
                             <input

                                 type = "text"
                                 onchange = {
                                    link.callback(|e: Event| {
                                        let target: EventTarget = e.target().unwrap();
                                        Msg::SetString(target.unchecked_into::<HtmlInputElement>().value())
                                    })
                                 }
                             />
                         </div>
                         <button
                             onclick = {
                                 link.callback(|_| Msg::Create)
                             }
                         > { fl!("create") } </button>
                    </div>
                </div>
            }
        }
    }
}
