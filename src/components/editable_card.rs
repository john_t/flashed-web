use yew::prelude::*;
use yew_lucide::{TextCursorInput, Trash2 as Trash};

use crate::components::TextAreaPP;
use crate::fl;

#[derive(Properties, PartialEq)]
pub struct Props {
    pub question: String,
    pub answer: String,
    pub input: bool,
    #[prop_or_default]
    pub on_edit_question: Option<Callback<String>>,
    #[prop_or_default]
    pub on_edit_answer: Option<Callback<String>>,
    #[prop_or_default]
    pub on_input_toggled: Option<Callback<()>>,
    #[prop_or_default]
    pub on_remove: Option<Callback<()>>,
}

pub struct EditableCard;

pub enum Msg {
    EditQuestion(String),
    EditAnswer(String),
    ToggleInput,
    Remove,
}

impl Component for EditableCard {
    type Message = Msg;
    type Properties = Props;

    fn create(_ctx: &Context<Self>) -> Self {
        Self
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        let props = ctx.props();
        match msg {
            Msg::EditQuestion(s) => {
                if let Some(on_edit_question) = &props.on_edit_question {
                    on_edit_question.emit(s);
                }
                false
            }
            Msg::EditAnswer(s) => {
                if let Some(on_edit_answer) = &props.on_edit_answer {
                    on_edit_answer.emit(s);
                }
                false
            }
            Msg::ToggleInput => {
                if let Some(on_input_toggled) = &props.on_input_toggled {
                    on_input_toggled.emit(());
                }
                false
            }
            Msg::Remove => {
                if let Some(on_remove) = &props.on_remove {
                    on_remove.emit(());
                }
                false
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();
        let props = ctx.props();

        html! {
            <div class = "edit-card">
                <label class = "question"> { fl!("question") } </label>
                <TextAreaPP
                    value = { props.question.clone() }
                    onchange = { link.callback(Msg::EditQuestion) }
                />
                <label class = "answer"> { fl!("answer") } </label>
                <TextAreaPP
                    value = { props.answer.clone() }
                    onchange = { link.callback(Msg::EditAnswer) }
                />
                <div class = "flat-button-container fullwidth">
                    <div class = "toggle-button">
                        <input type = "checkbox" checked = { props.input } aria-label = { fl!("card-toggle-input") } onclick = { link.callback(move |_| Msg::ToggleInput)}/>
                        <div class = "inner"> <TextCursorInput size = "15px" /> </div>
                    </div>
                    <button aria-label = { fl!("card-delete") } onclick = {link.callback(move |_| Msg::Remove)}> <Trash size = "15px"/> </button>
                </div>
            </div>
        }
    }
}
