use crate::pages::{Page, UrlPath};
use crate::util;
use yew::prelude::*;
use yew_lucide::{Copy, Home};
use yew_router::prelude::*;

#[derive(Properties, PartialEq, Debug)]
pub struct Props {
    pub path: UrlPath,
    #[prop_or_default]
    pub has_copy: bool,
}

#[function_component]
pub fn Path(props: &Props) -> Html {
    let copy = {
        let path = props.path.to_string();
        Callback::from(move |_| {
            util::copy(&path);
        })
    };

    html! {
        <div class="path-container">
            <ul class="path">
                <li>
                    <Link<Page> to = { Page::Opts }>
                        <Home/>
                    </Link<Page>>
                </li>
                {
                    props.path.0.iter().enumerate().map(|(i, seg)| {
                        let path = UrlPath(props.path.0[..=i].to_vec());
                        let to = Page::Folder { path };
                        html! {
                            <li>
                                <Link<Page> {to}> { &seg.0 } </Link<Page>>
                            </li>
                        }
                    }).collect::<Html>()
                }
            </ul>
            if props.has_copy {
                <button class="control" onclick = {copy}>
                    <Copy/>
                </button>
            }
        </div>
    }
}
