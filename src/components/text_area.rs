use wasm_bindgen::JsCast;
use web_sys::{EventTarget, HtmlTextAreaElement};
use yew::prelude::*;

#[derive(PartialEq, Properties)]
pub struct Props {
    #[prop_or_default]
    pub value: String,
    #[prop_or_default]
    pub onchange: Option<Callback<String>>,
}

pub struct TextAreaPP;

pub struct Msg(pub String);

impl Component for TextAreaPP {
    type Message = Msg;
    type Properties = Props;

    fn create(_ctx: &Context<Self>) -> Self {
        Self
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        if let Some(onchange) = &ctx.props().onchange {
            onchange.emit(msg.0);
        }
        false
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();
        let props = ctx.props();

        html! {
            <textarea
                value = { props.value.clone() }
                onchange = {
                    link.batch_callback(move |e: Event| {
                        let target: Option<EventTarget> = e.target();
                        let input = target.and_then(|t| t.dyn_into::<HtmlTextAreaElement>().ok());

                        input.map(|input| Msg(input.value()))
                    })
                }
            />
        }
    }
}
