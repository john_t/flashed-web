use yew::prelude::*;

#[derive(Properties, PartialEq)]
pub struct Props {
    pub total: f32,
    pub current: f32,
    #[prop_or_default]
    pub class: Classes,
}

#[function_component]
pub fn Progress(props: &Props) -> Html {
    html! {
        <div class={classes!("progress", props.class.clone())}>
            <div class="inner" style={ format!("width:{}%;", 100.0 * props.current / props.total) }/>
            <div class="content">
                <span> { format!("{} / {}", props.current, props.total) } </span>
            </div>
        </div>
    }
}
