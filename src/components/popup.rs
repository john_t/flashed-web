use enclose::enclose;
use yew::prelude::*;
use yew_hooks::prelude::*;

#[derive(Properties, PartialEq, Clone)]
pub struct PUProps {
    pub visible: bool,
    pub children: Children,
    pub onclickaway: Callback<()>,
}

#[function_component]
pub fn PopUp(props: &PUProps) -> Html {
    let node = use_node_ref();
    {
        let onclickaway = props.onclickaway.clone();
        use_click_away(node.clone(), move |_| onclickaway.clone().emit(()));
    }
    if props.visible {
        html! {
            <div class = "popup" ref = {node}>
                { for props.children.iter() }
            </div>
        }
    } else {
        html! {}
    }
}

#[derive(Properties, PartialEq, Clone)]
pub struct PUBProps {
    pub children: Children,
    #[prop_or_default]
    pub aria_label: String,
}

#[function_component]
pub fn PopUpButton(props: &PUBProps) -> Html {
    let visible = use_state(|| false);
    let onclick =
        Callback::from(enclose!((visible) move |_| visible.set(!*visible)));
    let onclickaway =
        Callback::from(enclose!((visible) move |_| visible.set(false)));
    html! {
        <div class="popup-button-holder" aria-label={props.aria_label.clone()}>
            <button {onclick} class="popup-button">
                { props.children.iter().next() }
            </button>
            <PopUp visible = {*visible} {onclickaway}>
                { for props.children.iter().skip(1) }
            </PopUp>
        </div>
    }
}
