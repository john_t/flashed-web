use crate::components::PopUpButton;
use yew::prelude::*;
use yew_lucide::{Edit3 as Edit, Layers, MoreHorizontal};

use crate::fl;

pub enum Msg {
    Selected,
    Edit,
    Remove,
    DeckMove,
    Rename,
    Stats,
    Download,
    DownloadScores,
}

#[derive(PartialEq, Properties)]
pub struct Props {
    pub name: String,
    pub onselected: Option<Callback<()>>,
    pub onedit: Option<Callback<()>>,
    pub onremove: Option<Callback<()>>,
    pub onmove: Option<Callback<()>>,
    pub onrename: Option<Callback<()>>,
    pub onstats: Option<Callback<()>>,
    pub ondownload: Option<Callback<()>>,
    pub ondownloadscores: Option<Callback<()>>,
}

#[derive(Default)]
pub struct Deck;

impl Component for Deck {
    type Message = Msg;
    type Properties = Props;

    fn create(_ctx: &Context<Self>) -> Self {
        Self
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::Edit => {
                if let Some(onedit) = &ctx.props().onedit {
                    onedit.emit(());
                }
                false
            }
            Msg::Remove => {
                if let Some(onremove) = &ctx.props().onremove {
                    onremove.emit(());
                }
                false
            }
            Msg::Selected => {
                if let Some(onselected) = &ctx.props().onselected {
                    onselected.emit(());
                }
                false
            }
            Msg::DeckMove => {
                if let Some(onmove) = &ctx.props().onmove {
                    onmove.emit(());
                }
                true
            }
            Msg::Rename => {
                if let Some(onrename) = &ctx.props().onrename {
                    onrename.emit(());
                }
                true
            }
            Msg::Stats => {
                if let Some(onstats) = &ctx.props().onstats {
                    onstats.emit(());
                }
                true
            }
            Msg::Download => {
                if let Some(ondownload) = &ctx.props().ondownload {
                    ondownload.emit(());
                }
                true
            }
            Msg::DownloadScores => {
                if let Some(ondownload) = &ctx.props().ondownload {
                    ondownload.emit(());
                }
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();
        let props = ctx.props();

        html! {
            <div class = "file">
                <div>
                    <Layers/>
                </div>
                <button
                    class = "main"
                    onclick = {link.callback(|_| Msg::Selected)}
                >
                    { props.name.clone() }
                </button>
                <PopUpButton aria_label = { fl!("more") } >
                    <MoreHorizontal/>
                    <button onclick = {link.callback(|_| Msg::Remove)} class = "destructive-action"> {fl!("delete")} </button>
                    <button onclick = {link.callback(|_| Msg::DeckMove)} > {fl!("move")} </button>
                    <button onclick = {link.callback(|_| Msg::Rename)} > {fl!("rename")} </button>
                    <button onclick = {link.callback(|_| Msg::Stats)} > {fl!("stats")} </button>
                    <button onclick = {link.callback(|_| Msg::Download)} > {fl!("download")} </button>
                    <button onclick = {link.callback(|_| Msg::DownloadScores)} > {fl!("download-scores")} </button>

                </PopUpButton>
                <button aria-label = { fl!("edit") } onclick = {link.callback(|_| Msg::Edit)}> <Edit/> </button>
            </div>
        }
    }
}
