use crate::components::PopUpButton;
use crate::fl;
use yew::prelude::*;
use yew_lucide::{Folder as FolderIcon, MoreHorizontal, Play};

#[derive(PartialEq, Properties)]
pub struct Props {
    pub name: String,
    pub onselected: Callback<()>,
    pub onremove: Callback<()>,
    pub onmove: Callback<()>,
    pub onrename: Callback<()>,
    pub onplay: Callback<()>,
    pub ondownload: Callback<()>,
    pub ondownloadscores: Callback<()>,
}

#[function_component]
pub fn Folder(props: &Props) -> Html {
    let onselected = props.onselected.clone();
    let onremove = props.onremove.clone();
    let onmove = props.onmove.clone();
    let onrename = props.onrename.clone();
    let onplay = props.onplay.clone();
    let ondownload = props.ondownload.clone();
    let ondownloadscores = props.ondownloadscores.clone();

    html! {
        <div class = "file">
            <div>
                <FolderIcon/>
            </div>
            <button class = "main" onclick = { move |_| onselected.clone().emit(()) }>
                <span> {
                    props.name.clone()
                } </span>
            </button>
            <PopUpButton aria_label = { fl!("more") } >
                <MoreHorizontal/>
                <button onclick = {move |_| onremove.clone().emit(())} class = "destructive-action"> {fl!("delete")} </button>
                <button onclick = {move |_| onmove.clone().emit(())}> {fl!("move")} </button>
                <button onclick = {move |_| onrename.clone().emit(())}> {fl!("rename")} </button>
                <button onclick = {move |_| ondownload.clone().emit(())}> {fl!("download")} </button>
                <button onclick = {move |_| ondownloadscores.clone().emit(())}> {fl!("download-scores")} </button>
            </PopUpButton>
            <button onclick = {move |_| onplay.clone().emit(())} aria-label = { fl!("edit") } > <Play/> </button>
        </div>
    }
}
