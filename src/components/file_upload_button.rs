use gloo_file::{callbacks::FileReader, File};
use web_sys::HtmlInputElement;
use yew::prelude::*;
use yew::{html, Callback, Component, Context, Event, Html, Properties};

pub enum Msg {
    File(Option<File>),
    Loaded { name: String, data: String },
    Clear,
}

#[derive(PartialEq, Properties)]
pub struct Props {
    /// What to do when loaded?
    ///
    /// Gives a name and data (in the order).
    pub onload: Callback<(String, String)>,
    pub accept: String,
    pub children: Children,
    #[prop_or_default]
    pub class: Classes,
}

pub struct FileUploadButton {
    pub name: Option<String>,
    pub loaded: Option<String>,
    reader: Option<FileReader>,
}

impl Component for FileUploadButton {
    type Message = Msg;
    type Properties = Props;

    fn create(_ctx: &Context<Self>) -> Self {
        FileUploadButton {
            name: None,
            loaded: None,
            reader: None,
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        let res = match msg {
            Msg::File(f) => {
                log::info!("{:?}", f);
                if let Some(file) = f {
                    let task = {
                        let link = ctx.link().clone();
                        let file_name = file.name();

                        gloo_file::callbacks::read_as_text(&file, move |res| {
                            link.send_message(Msg::Loaded {
                                data: res.unwrap_or_else(|e| e.to_string()),
                                name: file_name,
                            });
                        })
                    };
                    self.reader = Some(task);
                }
                false
            }
            Msg::Loaded { name, data } => {
                ctx.props().onload.emit((name, data));
                self.reader = None;

                true
            }
            Msg::Clear => {
                self.name = None;
                self.loaded = None;
                self.reader = None;

                true
            }
        };

        res
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();
        let props = ctx.props();
        let mut class = props.class.clone();
        class.push("file-upload-button");

        html! {
            <button {class}>
                <label for="file-input"> { for props.children.iter() } </label>
                <input type="file" id="file-input" accept= { props.accept.clone() } onchange = {
                    link.callback(move |e: Event| {
                        let mut result = None;
                        let input: HtmlInputElement = e.target_unchecked_into();

                        if let Some(files) = input.files() {
                            let mut files = js_sys::try_iter(&files)
                                .unwrap()
                                .unwrap()
                                .map(|v| web_sys::File::from(v.unwrap()))
                                .map(File::from);
                            result = files.next();
                        }

                        Msg::File(result)
                    })
                }/>
            </button>
        }
    }
}
