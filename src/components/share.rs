use percent_encoding::{utf8_percent_encode, AsciiSet, CONTROLS};
use wasm_bindgen_futures::spawn_local;
use yew::prelude::*;
use yew_lucide::Share as ShareIcon;

use crate::fl;
use crate::util;

const FRAGMENT: &AsciiSet =
    &CONTROLS.add(b' ').add(b'"').add(b'<').add(b'>').add(b'`');

#[derive(Properties, PartialEq, Clone)]
pub struct Props {
    pub name: String,
    pub file: String,
}

#[function_component(Share)]
pub fn share(props: &Props) -> Html {
    let show_link = use_state(|| false);

    // Create the url
    let window = web_sys::window().unwrap();
    let mut url = window.location().origin().unwrap_or_default();
    url.push_str(&yew_router::utils::fetch_base_url().unwrap_or_default());
    url.push_str("/upload/");
    let percent_encoded_name =
        utf8_percent_encode(&props.name, FRAGMENT).to_string();
    url.push_str(&percent_encoded_name);
    url.push('/');
    let percent_encoded_file =
        utf8_percent_encode(&props.file, FRAGMENT).to_string();
    url.push_str(&percent_encoded_file);

    html! {
        <>
            <button
                aira-label = { fl!("share") }
                onclick = {
                    let show_link = show_link.clone();
                    let url = url.clone();
                    let name = props.name.clone();
                    Callback::from(move |_| {
                        let name = name.clone();
                        let url = url.clone();
                        let show_link = show_link.clone();

                        spawn_local(async move {
                            let res = util::share::share("Flashed", &name, &url).await;
                            if !res.as_bool().unwrap() {
                                show_link.set(*show_link ^ true);
                            }
                        });
                    })
                }
            >
                <ShareIcon size = "15px"/>
            </button>
            if *show_link {
                <div class = "popup">
                    <div class = "popup-child">
                        <input
                            type = "text"
                            readonly = { true }
                            value = { url }
                        />
                        <p> { fl!("copy-me") } </p>
                    </div>
                </div>
            }
        </>
    }
}
