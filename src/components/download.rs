use yew::prelude::*;

#[derive(Properties, PartialEq)]
pub struct Props {
    pub children: Children,
    pub file_name: String,
    pub file: String,
}

#[function_component(Download)]
pub fn download(props: &Props) -> Html {
    html! {
        <div>
            <a class = "button" aria-label = "Download" download = { props.file_name.clone() } href = {
                use gloo_file::Blob;

                

                // Creates a browser blob  
                let blob = Blob::new_with_options(
                    props.file.as_str(),
                    Some("application/json"),
                );

                // Turns it into a URL
                let url = web_sys::Url::create_object_url_with_blob(
                    blob.as_ref(),
                ).unwrap();
                url
            }>
            { props.children.clone() }
            </a>
        </div>
    }
}
