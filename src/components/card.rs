use std::path::PathBuf;

use yew::prelude::*;
use yew::{html, Component, Context, Html, Properties};
use yew_lucide::{Check, CheckCircle, ChevronRight, XCircle, X as Cross};

use crate::components::{Markdown, TextAreaPP};
use crate::fl;

pub enum Msg {
    Flip(bool),
    Promote,
    Demote,
    TextChange(String),
}

#[derive(Properties, PartialEq)]
pub struct Props {
    pub question: String,
    pub answer: String,
    #[prop_or_default]
    pub score: Option<usize>,
    pub input: bool,
    #[prop_or_default]
    pub on_promote: Option<Callback<()>>,
    #[prop_or_default]
    pub on_demote: Option<Callback<()>>,
    #[prop_or_default]
    pub flipped: bool,
    pub path: Option<PathBuf>,
}

#[allow(clippy::module_name_repetitions)]
pub struct PlayingCard {
    pub flipped: bool,
    pub text: String,
    show_card_back: bool,
}

impl Component for PlayingCard {
    type Message = Msg;
    type Properties = Props;

    fn create(ctx: &Context<Self>) -> Self {
        Self {
            flipped: ctx.props().flipped,
            show_card_back: true,
            text: String::new(),
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::Flip(flip) => {
                self.show_card_back = true;
                if self.flipped == flip {
                    false
                } else {
                    self.flipped = flip;
                    true
                }
            }
            Msg::Promote => {
                // Emit the signle
                if let Some(on_promote) = &ctx.props().on_promote {
                    on_promote.emit(());
                }

                // Flip the card
                self.flipped = false;
                self.show_card_back = false;
                self.text = String::new();
                true
            }
            Msg::Demote => {
                // Emit the signle
                if let Some(on_demote) = &ctx.props().on_demote {
                    on_demote.emit(());
                }

                // Flip the card
                self.flipped = false;
                self.show_card_back = false;
                self.text = String::new();
                true
            }
            Msg::TextChange(x) => {
                self.text = x;
                false
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();

        // Allows the card to be visually flipped
        let mut card_classes = classes!("playing-card");
        if self.flipped {
            card_classes.extend(classes!("flipped"));
        }
        if self.show_card_back || self.flipped {
            card_classes.extend(classes!("showback"));
        }

        let props = ctx.props();

        html! {
            <div class = { card_classes }>
                <div class = "inner">
                    <div class = "front">
                        <div class = "markdown">
                            <Markdown string = { props.question.clone() }/>
                        </div>
                        { if props.input {
                            html!{
                                <>
                                <TextAreaPP value = { self.text.clone() } onchange = {link.callback(Msg::TextChange)}/>
                                <div class = "button-container">
                                    <button onclick={link.callback(|_| Msg::Flip(true))}> { fl!("check") }  </button>
                                </div>
                            </>}
                        } else {
                            html!{ <div class = "button-container">
                                <button onclick={link.callback(|_| Msg::Flip(true))}> { fl!("flip") } </button>
                            </div>}
                        }}
                        <div class="card-info-bar">
                            if let Some(score) = props.score {
                                <span> { score } </span>
                            }
                            if let Some(path) = &props.path {
                                <span class="card-info-path"> { path.to_str().unwrap() } </span>
                            }
                        </div>
                    </div>
                    <div class = "back"> {
                        if props.input {
                            html! {
                                <>
                                    <div class = "markdown slim">
                                        <Markdown string = { props.answer.clone() }/>
                                    </div>
                                    <div class = "markdown slim user-answer">
                                        <Markdown string = { self.text.clone() }/>
                                    </div>
                                    {
                                        if props.answer == self.text {
                                            html! {
                                                <div class="question-results correct">
                                                    <CheckCircle size = "48px"/>
                                                    <span> { fl!("correct") } </span>
                                                    <button aria-label = { fl!("next") } onclick = {link.callback(|_| Msg::Promote)}> <ChevronRight/> </button>
                                                </div>
                                            }
                                        } else {
                                            html! {
                                                <div class="question-results incorrect">
                                                    <XCircle size = "48px"/>
                                                    <span> { fl!("incorrect") } </span>
                                                    <button aria-label = { fl!("next") } onclick = {link.callback(|_| Msg::Demote)}> <ChevronRight/> </button>
                                                </div>
                                            }
                                        }
                                    }

                                </>
                            }
                        } else {
                            html! {
                                <>
                                    <div class = "markdown">
                                        <Markdown string = { props.answer.clone() }/>
                                    </div>
                                    <div class = "button-container">
                                        <button aria-label = { fl!("incorrect") } onclick={link.callback(|_| Msg::Demote)}> <Cross/> </button>
                                        <button onclick={link.callback(|_| Msg::Flip(false))}> { fl!("flip") } </button>
                                        <button aria-label = { fl!("correct") } onclick={link.callback(|_| Msg::Promote)}> <Check/> </button>
                                    </div>
                                </>
                            }
                        }
                    }
                        <div class="card-info-bar">
                            if let Some(score) = props.score {
                                <span> { score } </span>
                            }
                            if let Some(path) = &props.path {
                                <span class="card-info-path"> { path.to_str().unwrap() } </span>
                            }
                        </div>
                    </div>
                </div>
            </div>
        }
    }
}
