use super::Progress;
use yew::{html, Html};

#[must_use]
pub fn info_bar(app: &flashed::App) -> Html {
    html! {
        // <div class="bar bottom-bar">
            <Progress total = { app.cards().len() as f32 } current = { (app.cards().len() - app.unfinished_count()) as f32 }/>
        //     <span> { fl!("info-card-score", value = app.card_score()) } </span>
        //     <span> { fl!("info-correct", value = (app.cards().len() - app.unfinished_count())) } </span>
        //     <span> { fl!("info-remaining", value = app.unfinished_count()) } </span>
        //     <span> { fl!("info-total", value = app.cards().len()) } </span>
        // </div>
    }
}
