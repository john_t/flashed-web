# Flashed Web

[![pipeline status](https://gitlab.com/john_t/flashed-web/badges/master/pipeline.svg)](https://gitlab.com/john_t/flashed-web/-/commits/master)

This is a web PWA that you can use with flashed. You can create or upload
flashcards and run through the deck. Its licensed under the AGPL-3.0.

Play [here](https://john_t.gitlab.io/flashed-web/).
