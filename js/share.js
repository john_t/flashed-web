export async function share(title, text, url) {
    let shareData = {
        title: title,
        text: text,
        url: url,
    }
    try {
        await navigator.share(shareData);
        return true;
    } catch(err) {
        return false;
    }
}
