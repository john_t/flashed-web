"use strict";

export function panic(msg) {
    console.error(msg);

    let data = new Blob([msg], { type: "text/plain" } );
    var url = window.URL.createObjectURL(data);

    var a_element = document.createElement('a');
    var a_content = document.createTextNode("Crashed!, download logfiler");
    a_element.appendChild(a_content);
    a_element.setAttribute('href', url);
    a_element.setAttribute('download', 'log.txt');
    document.body.appendChild(a_element);

    var p_element = document.createElement('p');
    var p_content = document.createTextNode(msg);
    p_element.appendChild(p_content);
    document.body.appendChild(p_element);
    
    element.click();
}
