// Change the theme-color to match theme
if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
    console.log("Using dark theme");
    document.querySelector('meta[name="theme-color"]').setAttribute('content', '#2ec27e');
} else {
    console.log("Using light theme");
    document.querySelector('meta[name="theme-color"]').setAttribute('content', '#7ba84d');
}

if ('serviceWorker' in navigator) {
    console.log("Running as a PWA")
    navigator.serviceWorker.register('/flashed-web/sw.js')
        .then(reg => console.log('service worker registered'))
        .catch(err => console.log('service worker not registered', err));
}
